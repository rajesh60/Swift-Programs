//
//  SwiftFunctions.swift
//  UnitTestingExample
//
//  Created by techno krishna on 12/03/16.
//  Copyright © 2016 techno krishna. All rights reserved.
//

import Foundation


func minMax(arr:[Int]) -> (max:Int,min:Int) {
    assert(arr != [])
    var small = arr[0],big = arr[0]
    for i in arr {
        if big < i {
            big = i
        }
        if small > i {
           small = i
        }
    }
    return (big,small)
}

func isPrime(n:Int)->Bool {
    
    if n < 2 {
        return false
    }
    for i in 2..<n {
        
        if n % i == 0 {
            return false
        }
    }
    return true
}

func gcd(var x:Int, var _ y:Int)->Int
{
    while y != 0 {
        let z = y
        y = x % y
        x = z
    }
    return x
}

func indexOf(arr:[Int],n:Int)->Int? {
    
    assert(arr != [])
    for i in 0..<arr.count {
        if n == arr[i] {
            return i
        }
    }
    return nil
}

func reverse(arr:[Int])->[Int] {
    
    if arr  == [] {
        return arr
    }
    var empty = [Int]()
    for var i = arr.count-1;i>=0;i-- {
        empty.append(arr[i])
    }
    return empty
}

func removeDuplicates(arr:[Int])->[Int] {
    var newArr = [Int]()
    
    if arr != []
    {
        newArr.append(arr[0])
        
        for i in 1..<arr.count {
            
            if arr[i] != arr[i-1]
            {
                newArr.append(arr[i])
            }
        }
        return newArr
    }
    return arr
}









