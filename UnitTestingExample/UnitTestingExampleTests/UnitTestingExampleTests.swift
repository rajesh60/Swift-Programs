//
//  UnitTestingExampleTests.swift
//  UnitTestingExampleTests
//
//  Created by techno krishna on 12/03/16.
//  Copyright © 2016 techno krishna. All rights reserved.
//

import XCTest

class UnitTestingExampleTests: XCTestCase {
    func testExample() {
        XCTAssert(true)
    }
    
    func testminMax()
    {
        
        let arr = minMax([2,3,5,7,11,13])
        
        XCTAssertEqual(arr.max, 13)
        XCTAssertEqual(arr.min, 2)
        
        XCTAssertFalse(arr.min == 0)
        XCTAssertFalse(arr.max == 12) 
        
        let arr1 = minMax([5,4,3,2,1])
        
        XCTAssertEqual(arr1.max, 5)
        XCTAssertEqual(arr1.min, 1)
        
        let arr2 = minMax([5,4,2,3,9,8,7,11])
        
        XCTAssertEqual(arr2.max, 11)
        XCTAssertEqual(arr2.min, 2)
        
        
    }
    
    func testPrime()
    {
        XCTAssert(isPrime(5))
        XCTAssert(isPrime(7))
        XCTAssert(isPrime(11))
        
        XCTAssertFalse(isPrime(0))
        XCTAssertFalse(isPrime(1))
        XCTAssertFalse(isPrime(8))
    }
    
    func testgcd()
    {
        XCTAssertEqual(gcd(12, 5), 1)
        XCTAssertEqual(gcd(12, 4), 4)
        XCTAssertEqual(gcd(100, 0), 100)
        XCTAssertEqual(gcd(0, 100), 100)
        
        XCTAssertFalse(gcd(27, 9) == 3)
        XCTAssertFalse(gcd(100, 0) == 0)
    }
    
    func testIndexOf()
    {
        XCTAssertEqual(indexOf([1,2,3,4,5], n: 4), 3)
        XCTAssertEqual(indexOf([2,3,5,7,11 ], n: 11), 4)
        XCTAssertEqual(indexOf([0,2,4,6], n: 5), nil)
        
        
        
    }
    
    func testReverse()
    {
        XCTAssertEqual(reverse([1,2,3,4]),[4,3,2,1])
        XCTAssertEqual(reverse([2,3,5,7,11]), [11,7,5,3,2])
        XCTAssertEqual(reverse([]), [])
    
    }
    
    func testRemoveDuplecates()
    {
        
        XCTAssertEqual(removeDuplicates([1,2,2,3,4,4,5]), [1,2,3,4,5])
        XCTAssertEqual(removeDuplicates([2,3,5,5,7,7]), [2,3,5,7])
        
        XCTAssertEqual(removeDuplicates([]),[])
    }

    
}

